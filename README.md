#阿里巴巴开放平台接口对接SDK
composer 安装命令
```composer
composer require arkizat/ali_open
```
```php
<?php

use Arkizat\AliOpen\AliOpen;

require_once 'vendor/autoload.php';
$param = [
    'groupId'=>241,
    'pageNo'=>1,
    'pageSize'=>20,
    'feedId'=>'',
    'title'=>'',
];
$obj = new AliOpen($param);
$obj->setAppkey('123456');
$obj->setAppsecret('ddee2112');
$obj->setAccessToken('721334528-581f-4508-98uy-a646a1hy65fa');
try {
    $res = $obj->goods->listCybUserGroupFeed()->get();
    echo json_encode($res);
} catch (Exception $e) {
    echo json_encode(['code'=>400,'msg'=>$e->getMessage()]);
}


```
##引入很简单composer 安装后调用就可以
