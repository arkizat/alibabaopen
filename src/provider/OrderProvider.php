<?php


namespace Arkizat\AliOpen\provider;


use Arkizat\AliOpen\core\Container;
use Arkizat\AliOpen\functions\Order;
use Arkizat\AliOpen\interfaces\Provider;

/**
 * 订单服务提供者
 */
class OrderProvider implements Provider
{
    public function serviceProvider(Container $container)
    {
        $container['order'] = function ($container){
            return new Order($container);
        };
    }
}
