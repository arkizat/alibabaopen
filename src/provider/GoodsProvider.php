<?php


namespace Arkizat\AliOpen\provider;


use Arkizat\AliOpen\core\Container;
use Arkizat\AliOpen\functions\Goods;
use Arkizat\AliOpen\interfaces\Provider;

/**
 * 商品服务提供者
 */
class GoodsProvider implements Provider

{

    public function serviceProvider(Container $container)
    {
        $container['goods'] = function ($container){
            return new Goods($container);
        };
    }
}
