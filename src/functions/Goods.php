<?php

namespace Arkizat\AliOpen\functions;
use Arkizat\AliOpen\core\BaseClient;

/**
 * class Goods
 */
class Goods extends BaseClient
{
    /**
     * 获取选品库已选商品列表
     * @return Goods
     */
    public function listCybUserGroupFeed(): Goods
    {
        return $this->setApi('com.alibaba.p4p:alibaba.cps.op.listCybUserGroupFeed-1');
    }

    /**
     * 获取我的选品库列表
     * @return Goods
     */
    public function listCybUserGroup(): Goods
    {
        return $this->setApi('com.alibaba.p4p:alibaba.cps.op.listCybUserGroup-1');
    }

    /**
     * 获取商品详情接口
     * @return Goods
     */
    public function productInfo(): Goods
    {
        return $this->setApi('com.alibaba.product:alibaba.cpsMedia.productInfo-1');
    }

    /**
     * 商品列表搜索接口
     * @return Goods
     */
    public function searchCybOffers(): Goods
    {
        return $this->setApi('com.alibaba.p4p:alibaba.cps.op.searchCybOffers-1');
    }




}
