<?php

namespace Arkizat\AliOpen\functions;
use Arkizat\AliOpen\core\BaseClient;

class Order extends BaseClient
{
    /**
     * 溢价模式订单创建接口
     * @return $this
     */
    public function createOrder4CybMedia(): Order
    {
        return $this->setApi('com.alibaba.trade:alibaba.trade.createOrder4CybMedia-1');
    }

    /**
     * 溢价模式创建订单前预览数据接口
     */
    public function preview4CybMedia(): Order
    {
        return $this->setApi('com.alibaba.trade:alibaba.createOrder.preview4CybMedia-1');
    }

    /**
     * 取消交易
     */
    public function cancel(): Order
    {
        return $this->setApi('com.alibaba.trade:alibaba.trade.cancel-1');
    }

    /**
     * 批量获取订单的支付链接
     */
    public function getAlpayUrl(): Order
    {
        return $this->setApi('com.alibaba.trade:alibaba.alipay.url.get-1');
    }

    /**
     * 支付宝协议代扣支付
     */
    public function protocolPay(): Order
    {
        return $this->setApi('com.alibaba.trade:alibaba.trade.pay.protocolPay-1');
    }

}
