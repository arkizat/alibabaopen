<?php

namespace Arkizat\AliOpen\core;


/**
 * 处理主类
 */
class ContainerBase extends Container
{
    protected $provider = [];

    public $params = [];

    public $baseUrl = 'https://gw.open.1688.com/openapi/';

    public $appKey='';

    public $appSecret='';

    public $accessToken = '';

    /**
     * 构造方法
     * @param array $params
     */
    public function __construct(array $params = [])
    {
        $this->params = $params;
        $provider_callback = function ($provider) {
            $obj =new $provider;
            $this->serviceRegister($obj);
        };
        //注册服务者
        array_walk($this->provider, $provider_callback);
    }

    /**
     * 获取器
     * @param $id
     * @return mixed
     */
    public function __get($id)
    {
        return $this->offsetGet($id);
    }

    /**
     * App Secret
     * @param $appSecret
     */
    public function setAppSecret($appSecret): void
    {
        $this->appSecret = $appSecret;
    }

    /**
     * App Key
     * @param $appKey
     */
    public function setAppKey($appKey): void
    {
        $this->appKey = $appKey;
    }

    /**
     * access Token
     * @param $accessToken
     */
    public function setAccessToken($accessToken): void
    {
        $this->accessToken = $accessToken;
    }

    /**
     * BaseUrl
     * @param $BaseUrl
     */
    public function setBaseUrl($BaseUrl): void
    {
        $this->baseUrl = $BaseUrl;
    }
}
