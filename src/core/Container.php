<?php
namespace Arkizat\AliOpen\core;


/**
 * Class Container
 */
class Container implements \ArrayAccess
{
    /**
     * 中间件
     * @var array
     */
    protected $middlewares = [];
    private $instances =[];
    private $values = [];

    public $register;

    /**
     * 注册服务者
     * @param $provider
     * @return $this
     */
    public function serviceRegister($provider): Container
    {
        $provider->serviceProvider($this);

        return $this;
    }

    /**
     * 是否存在服务者
     * @param mixed $offset
     * @return bool|void
     */
    public function offsetExists($offset): bool
    {
        return array_key_exists($offset, $this->values);
    }

    /**
     * 获取服务者
     * @param mixed $offset
     * @return mixed
     */
    public function offsetGet($offset): mixed
    {
        if(isset($this->instances[$offset])){
            return $this->instances[$offset];
        }
        $raw = $this->values[$offset];
        $val = $this->values[$offset] = $raw($this);
        $this->instances[$offset] = $val;
        return $val;
    }

    /**
     * 设置服务者
     * @param mixed $offset
     * @param mixed $value
     */
    public function offsetSet($offset, $value): void
    {
        $this->values[$offset] = $value;
    }

    /**
     * 删除服务者
     * @param mixed $offset
     */
    public function offsetUnset($offset): void
    {
        unset($this->values[$offset]);
    }

    /**
     * 获取中间件
     * @return array
     */
    public function getMiddlewares(): array
    {
        return $this->middlewares;
    }

    /**
     * 设置中间件
     * @param array $middlewares
     */
    public function setMiddlewares(array $middlewares)
    {
        $this->middlewares = $middlewares;
    }

    /**
     * 添加中间件
     * @param array $class_and_function
     * @param string $name
     * @return array
     */
    public function pushMiddlewares(array $class_and_function, string $name =''): array
    {
        if(empty($this->middlewares)){
            $this->middlewares[$name] = $class_and_function;
        }else{
            array_push($this->middlewares,[$name=>$class_and_function]);
        }
        return $this->middlewares;
    }


}
