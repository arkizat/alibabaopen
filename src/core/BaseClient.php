<?php
namespace Arkizat\AliOpen\core;

use Arkizat\AliOpen\AliOpen;

/**
 * Class BaseClient
 * @property  AliOpen app
 */
class BaseClient
{
    protected $app;

    public $urlInfo;

    protected $postData;

    public $resUrl;

    public function __construct(Container $app)
    {
        $this->app = $app;
    }

    /**
     * 生成签名
     * @throws Exception
     */
    public function sign(){
        //判断请求Url
        if(empty($this->urlInfo)){
            throw new Exception('请配置Api Url信息');
        }
        $arr = explode(':',$this->urlInfo);
        $spaceName = $arr[0];
        $arr = explode('-',$arr[1]);
        $version = $arr[1];
        $apiName = $arr[0];
        $url_info = 'param2/' . $version . '/' . $spaceName . '/' . $apiName . '/';

        //参数因子
        $apiInfo = $url_info . $this->app->appKey;
        //配置参数，请用apiInfo对应的api参数进行替换
        $code_arr = array_merge(['access_token' => $this->app->accessToken],$this->app->params);
        $aliParams = [];
        $url_pin = '';
        foreach ($code_arr as $key => $val) {
            $url_pin .=$key.'='.$val.'&';
            $aliParams[] = $key . $val;
        }
        sort($aliParams);
        $sign_str = join('', $aliParams);
        $sign_str = $apiInfo . $sign_str;

        //签名
        $code_sign = strtoupper(bin2hex(hash_hmac("sha1", $sign_str, $this->app->appSecret, true)));
        $this->postData  = $code_arr;
        $this->resUrl =  $this->app->baseUrl.$apiInfo.'?'.$url_pin.'_aop_signature='.$code_sign;
    }

    /**
     * get请求方式
     * @return mixed
     * @throws Exception
     */
    public function get()
    {
        $this->sign();
        $file = $this->curlRequest($this->resUrl,'','GET');
        return json_decode($file,true);
    }

    /**
     * post请求方式
     * @return mixed
     * @throws Exception
     */
    public function post()
    {
        $this->sign();
        $result = $this->curlRequest($this->resUrl,$this->postData,'POST');
        return json_decode($result, true);
    }

    /**
     * 设置地址
     * @param $comAlibabaApiUrl
     * @return $this
     */
    public function setApi($comAlibabaApiUrl): BaseClient
    {
        $this->urlInfo = $comAlibabaApiUrl;
        return $this;
    }

    /**
     * curl 请求
     * @param $base_url
     * @param $query_data
     * @param string $method
     * @param bool $ssl
     * @param int $exe_timeout
     * @param int $conn_timeout
     * @param int $dns_timeout
     * @return bool|string
     */
    public function curlRequest($base_url, $query_data, string $method = 'get', bool $ssl = true, int $exe_timeout = 10, int $conn_timeout = 10, int $dns_timeout = 3600)
    {

        $ch = curl_init();

        if ( $method == 'get' ) {
            //method get
            if ( ( !empty($query_data) )
                && ( is_array($query_data) )
            ){
                $connect_symbol = (strpos($base_url, '?')) ? '&' : '?';
                foreach($query_data as $key => $val) {
                    if ( is_array($val) ) {
                        $val = serialize($val);
                    }
                    $base_url .= $connect_symbol . $key . '=' . rawurlencode($val);
                    $connect_symbol = '&';
                }
            }
        } else {
            if ( ( !empty($query_data) )
                && ( is_array($query_data) )
            ){
                foreach($query_data as $key => $val) {
                    if ( is_array($val) ) {
                        $query_data[$key] = serialize($val);
                    }
                }
            }
            //method post
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $query_data);
        }

        curl_setopt($ch, CURLOPT_URL, $base_url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $conn_timeout);
        curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, $dns_timeout);
        curl_setopt($ch, CURLOPT_TIMEOUT, $exe_timeout);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        // 关闭ssl验证
        if($ssl){
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        }

        $output = curl_exec($ch);

        if ( $output === false)
            $output = '';

        curl_close($ch);
        return $output;
    }

}
