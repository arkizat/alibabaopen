<?php

namespace Arkizat\AliOpen;

use Arkizat\AliOpen\core\ContainerBase;
use Arkizat\AliOpen\functions\Goods;
use Arkizat\AliOpen\functions\Order;
use Arkizat\AliOpen\provider\OrderProvider;
use Arkizat\AliOpen\provider\GoodsProvider;

/**
 * Class Application
 * @property Order order
 * @property Goods goods
 */
class AliOpen extends ContainerBase
{
    /**
     * 服务提供者
     * @var array
     */
    public function __construct($params = [])
    {
        parent::__construct($params);
    }

    /**
     * 其他服务提供者
     * @var string[]
     */
    protected $provider = [
        OrderProvider::class,
        GoodsProvider::class,
    ];
}
