<?php
namespace Arkizat\AliOpen\interfaces;

use Arkizat\AliOpen\core\Container;

interface Provider
{
    public function serviceProvider(Container $container);
}
